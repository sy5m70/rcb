package com.ealynx.ems.basket;

import java.math.BigDecimal;

import junit.framework.Assert;

import org.junit.Test;

import com.ealynx.ems.basket.fruit.FruitVisitor;
import com.ealynx.ems.model.Fruit;

/**
 * Unit test for basket adding items and printing the total cost.
 */
public final class BasketTest {

	/**
	 * Calculate basket total price.
	 */
	@Test
	public void calculateBasketTotalAmount() {

		FruitVisitor visitor = new FruitVisitor();
		visitor.visit(new Fruit("apple", "1.20", 3));
		visitor.visit(new Fruit("pear", "1.35", 2));
		visitor.visit(new Fruit("orange", "0.80", 7));
		visitor.visit(new Fruit("lemon", "0.50", 5));
		visitor.visit(new Fruit("peach", "0.70", 6));

		Assert.assertEquals(new BigDecimal("18.60"), visitor.getGrandTotal());
		Assert.assertEquals(5, visitor.getItems().size());
	}
}
