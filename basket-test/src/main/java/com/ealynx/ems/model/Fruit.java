package com.ealynx.ems.model;

import java.math.BigDecimal;

import com.ealynx.ems.visitor.Visitable;
import com.ealynx.ems.visitor.Visitor;

/**
 * Fruit class contains all type of fruits with its price, quantity and name.
 */
public final class Fruit implements Visitable {

	/** fruit name such as apple, pear, orange. **/
	private String name;

	/** unit price of a fruit. **/
	private BigDecimal unitPrice;

	/** quantity - number of fruits. **/
	private int quantity;

	/** constructor to accept name, unit price and quantity of fruit. **/
	public Fruit(final String name, final String price, final int quantity) {
		this.unitPrice = new BigDecimal(price);
		this.name = name;
		this.quantity = quantity;
	}

	/**
	 * Get fruit name.
	 * 
	 * @return fruit name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get unit price of a fruit.
	 * 
	 * @return unit price
	 */
	public BigDecimal getPrice() {
		return unitPrice;
	}

	/**
	 * Get quantity in number.
	 * 
	 * @return quantity
	 */
	public int getQuantity() {
		return quantity;
	}
	
	public BigDecimal getCost(){
		return unitPrice.multiply(new BigDecimal(quantity));
	}

	/**
	 * Implement accept() of visit pattern.
	 */
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
}
