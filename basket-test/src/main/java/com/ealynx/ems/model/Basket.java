package com.ealynx.ems.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.ealynx.ems.visitor.Visitor;

/**
 * Basket contains all the items added to it and keep track of total cost.FOs
 */
public abstract class Basket<Visitable> implements Visitor {

	/** total cost of the basket items. **/
	protected BigDecimal totalAmount = new BigDecimal("0");

	/** list of all the items in the basket. **/
	protected List<Visitable> items = new ArrayList<>();

	/**
	 * Get total cost of the items within a basket.
	 * 
	 * @return cost or total amount
	 */
	public BigDecimal getGrandTotal() {
		return totalAmount;
	}

	/**
	 * Get items in the basket.
	 * 
	 * @return list of items
	 */
	public List<Visitable> getItems() {
		return items;
	}
}
