package com.ealynx.ems.basket.fruit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ealynx.ems.model.Basket;
import com.ealynx.ems.model.Fruit;
import com.ealynx.ems.visitor.Visitable;

/**
 * Fruit visitor class.
 */
public final class FruitVisitor extends Basket<Visitable> {

	private final static Logger LOGGER = LoggerFactory
			.getLogger(FruitVisitor.class);

	/**
	 * Visit fruit implementation.
	 */
	public void visit(Fruit fruit) {
		LOGGER.info("Visiting fruit {}, unit price {} and quantity {}.",
				fruit.getName(), fruit.getPrice(), fruit.getQuantity());

		totalAmount = totalAmount.add(fruit.getCost());
		items.add(fruit);
	}
}
