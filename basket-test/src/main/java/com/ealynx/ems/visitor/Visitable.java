package com.ealynx.ems.visitor;

/**
 * Visitor pattern so all items will implement accept.
 */
public interface Visitable {
	
	/** accept visitor interface. **/
	void accept(Visitor visitor);
}
