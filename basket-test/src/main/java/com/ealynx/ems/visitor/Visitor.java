package com.ealynx.ems.visitor;

import com.ealynx.ems.model.Fruit;

/**
 * Visitor pattern, basket item implement visit.
 */
public interface Visitor {

	/**
	 * Fruit visitor.
	 * 
	 * @param fruit
	 */
	void visit(final Fruit fruit);
}
